import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()


with open(os.path.join(here, 'requirements.txt')) as f:
    req_lines = [line.strip() for line in f.readlines()]

    def fix_url_line(line):
        if '#egg=' in line:
            return line.rsplit('#egg=', 1)[1]
        return line
    requires = [fix_url_line(line) for line in req_lines if line]


setup(
    name='kaiju_model',
    version='0.1.0',
    description='.',
    long_description=README + '\n\n' + CHANGES,
    long_description_content_type='text/markdown; charset=UTF-8',
    author='sergey.sargsyan',
    author_email='xmachina@ya.ru',
    url='https://bitbucket.org/market_app/kaiju-model',
    packages=find_packages(),
    package_dir={'kaiju_model': 'kaiju_model'},
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=requires,
    license='LICENSE.txt',
    zip_safe=False,
    classifiers=(
        'Programming Language :: Python :: 3.6',
    ),
    keywords=['tools', 'elemento', 'kaiju', 'rpc', 'jsonschema']
)
